package handlers

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestLandingResponseNotAllowedIfUsingSomethingDifferentThanGet(t *testing.T) {
	req, err := http.NewRequest(http.MethodPost, LandingPageUrl, nil)
	assert.Nil(t, err, "Should no have an error with the request")

	responseRecorder := httptest.NewRecorder()
	handlerUnderTest := Decorate(http.HandlerFunc(Landing), GetOnly())
	handlerUnderTest.ServeHTTP(responseRecorder, req)
	assert.Equal(t, http.StatusMethodNotAllowed, responseRecorder.Code)
}

func TestLandingRespondsAnOkStatus(t *testing.T) {
	req, err := http.NewRequest(http.MethodPost, LandingPageUrl, nil)
	assert.Nil(t, err, "Should no have an error with the request")

	responseRecorder := httptest.NewRecorder()
	handlerUnderTest := http.HandlerFunc(Landing)
	handlerUnderTest.ServeHTTP(responseRecorder, req)
	assert.Equal(t, http.StatusOK, responseRecorder.Code)
}

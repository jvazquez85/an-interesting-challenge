package handlers

import (
	"io"
	"log"
	"net/http"
)

func Landing(w http.ResponseWriter, req *http.Request) {
	log.Println("Received a request")
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, "Welcome to fake api page")
}

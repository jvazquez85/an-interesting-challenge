package handlers

import (
	"log"
	"net/http"

	"gitlab.com/jvazquez85/an-interesting-challenge/thirdparty/responses"
)

func CreateOrder(w http.ResponseWriter, req *http.Request) {
	log.Println("Received a request to create an order")
	randomResponse := responses.StubCases(responses.OkResponse)
	w.WriteHeader(randomResponse.StatusCode)
	log.Printf("Created order in shipstation")
}

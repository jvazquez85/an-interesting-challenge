package handlers

import (
	"fmt"
	"gitlab.com/jvazquez85/an-interesting-challenge/thirdparty/configuration"
	"golang.org/x/time/rate"
	"log"
	"net/http"
	"sync"
)

var Visitors = make(map[string]*rate.Limiter)
var mu sync.Mutex

type Decorator func(http.Handler) http.Handler

func Decorate(h http.Handler, decorators ...Decorator) http.Handler {
	// apply decorator backwards so that they are executed in declared order
	for i := len(decorators) - 1; i >= 0; i-- {
		h = decorators[i](h)
	}
	return h
}

func PostOnly() Decorator {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method == http.MethodPost {
				h.ServeHTTP(w, r)
				return
			}
			http.Error(w, "Method not allowed.", http.StatusMethodNotAllowed)
		})
	}
}

func GetOnly() Decorator {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method == http.MethodGet {
				h.ServeHTTP(w, r)
				return
			}
			http.Error(w, "Method not allowed.", http.StatusMethodNotAllowed)
		})
	}
}

func HandleAuth() Decorator {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			user, password, ok := r.BasicAuth()
			if !ok {
				w.Header().Set("WWW-Authenticate", `Basic realm="Provide your user and password"`)
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte("Unauthorized.\n"))
				return
			}

			if user != configuration.DevelopmentUser || password != configuration.DevelopmentPassword {
				w.Header().Set("WWW-Authenticate", `Basic realm="Provide your user and password"`)
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte("Unauthorized.\n"))
				return
			}

			h.ServeHTTP(w, r)
			return
		})
	}
}

// Retrieve and return the rate limiter for the current visitor if it
// already exists. Otherwise create a new rate limiter and add it to
// the Visitors map, using the IP address as the key.
func getVisitor(user string) *rate.Limiter {
	mu.Lock()
	defer mu.Unlock()
	//Or to describe it another way – the limiter permits you
	//to consume an average of r tokens per second,
	//with a maximum of b tokens in any single 'burst'.
	//So in the code above our limiter allows 1 token to be consumed per second,
	//with a maximum burst size of 3.

	// Shipstation rate limit 40 r x 1m
	limiter, exists := Visitors[user]
	if !exists {
		log.Panic(fmt.Sprintf("%s should never happen at this stage", user))
	}

	return limiter
}

func Limit() Decorator {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			user, _, _ := r.BasicAuth()
			// Call the getVisitor function to retreive the rate limiter for
			// the current user.
			limiter := getVisitor(user)
			if limiter.Allow() == false {
				http.Error(w, http.StatusText(http.StatusTooManyRequests),
					http.StatusTooManyRequests)
				log.Println("Rate limit reached, waiting")
				return
			}
			log.Print("Rate limit allowed")
			h.ServeHTTP(w, r)
		})
	}
}

package main

import (
	"fmt"
	"gitlab.com/jvazquez85/an-interesting-challenge/thirdparty/configuration"
	landing "gitlab.com/jvazquez85/an-interesting-challenge/thirdparty/handlers"
	"golang.org/x/time/rate"
	"log"
	"net/http"
	"os"
)

func main() {
	var servicePort string

	limiter := rate.NewLimiter(1, 3)
	landing.Visitors[configuration.DevelopmentUser] = limiter

	if port := os.Getenv("PORT"); port == "" {
		servicePort = configuration.DefaultPort
	} else {
		servicePort = port
	}

	log.Printf("Listening at %s", servicePort)

	http.Handle(landing.LandingPageUrl,
		landing.Decorate(http.HandlerFunc(landing.Landing), landing.GetOnly()))

	http.Handle(landing.OrderPageUrl,
		landing.Decorate(http.HandlerFunc(landing.CreateOrder),
			landing.HandleAuth(),
			landing.PostOnly(),
			landing.Limit()),
	)

	http.ListenAndServe(fmt.Sprintf(":%s", servicePort), nil)
}

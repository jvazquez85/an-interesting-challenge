package responses

import (
	"math/rand"
	"net/http"
	"time"
)

const OkResponse int = 1
const BadResponse int = 2
const ErrorResponse int = 3

type Response struct {
	Id         int
	StatusCode int
}

type UserLimit struct {
	XRateLimit          int
	XRateLimitRemaining int
	XRateLimitReset     int
}

func NewUserLimit() *UserLimit {
	return &UserLimit{
		XRateLimit:          40,
		XRateLimitRemaining: 40,
		XRateLimitReset:     0,
	}
}

// From the provided number, obtain the response
func StubCases(seed int) Response {
	var stubResponses []Response
	var response Response

	stubResponses = append(stubResponses,
		Response{Id: 1, StatusCode: http.StatusCreated},
		Response{Id: 2, StatusCode: http.StatusBadRequest},
		Response{Id: 3, StatusCode: http.StatusInternalServerError},
		Response{Id: 4, StatusCode: http.StatusTooManyRequests},
	)

	for _, stubCase := range stubResponses {
		if seed == stubCase.Id {
			response = stubCase
		}
	}

	if response.Id == 0 {
		response = stubResponses[2]
	}

	return response
}

func GetMeANumberInRange() int {
	rand.Seed(time.Now().Unix())
	rangeLower := 1
	rangeUpper := 5
	randomNum := rangeLower + rand.Intn(rangeUpper-rangeLower+1)
	return randomNum
}

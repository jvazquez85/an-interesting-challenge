package responses

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestResponseObtainsOkResponse(t *testing.T) {
	var response Response
	response = StubCases(OkResponse)
	assert.Equal(t, OkResponse, response.Id)
}

func TestUnknownNumberReturnsInternalError(t *testing.T) {
	response := StubCases(6)
	assert.Equal(t, ErrorResponse, response.Id)
}

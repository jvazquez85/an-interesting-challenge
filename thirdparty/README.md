# Initial rate limit

I just obtained the first response by doing

```shell
 curl -sSL -D - "https://user:password@ssapi.shipstation.com/accounts/listtags" -o /dev/null
```

which got me

```shell
HTTP/1.1 200 OK
cache-control: no-cache
content-type: application/json; charset=utf-8
date: Fri, 19 Feb 2021 08:06:30 GMT
expires: -1
pragma: no-cache
server: Microsoft-IIS/8.0
x-aspnet-version: 4.0.30319
x-newrelic-app-data: PxQGVFZXCgITVVVWAgUCV1EBFB9AMQYAZBBZDEtZV0ZaCldOZgRRIzR/GCAFAFdGXhASTXtYRBIwUFFAQEpTTABOCEwIDAEDBlQKTRxQH0BTUgVYX1EKUFIAWQcCBQ4ERh1QUg4VBj8=
x-powered-by: ASP.NET
X-Rate-Limit-Limit: 40
X-Rate-Limit-Remaining: 40
X-Rate-Limit-Reset: 30
Content-Length: 1755
Connection: keep-alive
```

# Testing limits

Install apache benchmark (ab)
```shell
ab -k -n 50 -c 10 -t 20 -A development:development -m POST http://localhost:8181/create-order/
```

package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestFakeServerReturnsAKnownResponse(t *testing.T) {
	req, err := http.NewRequest("GET", "/createOrder", nil)
	assert.Nil(t, err, "Should no have an error with the request")

	responseRecorder := httptest.NewRecorder()
	handlerUnderTest := http.HandlerFunc(fakeApi)
	handlerUnderTest.ServeHTTP(responseRecorder, req)
	switch responseRecorder.Code {
	case http.StatusCreated:
	case http.StatusInternalServerError:
	case http.StatusTooManyRequests:
	case http.StatusBadRequest:
		break
	default:
		assert.True(t, false, fmt.Sprintf("%d is not in the expected case",
			responseRecorder.Code))
	}
}

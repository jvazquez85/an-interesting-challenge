DOCKER = $(shell which docker)
BUILD_ARG = $(if $(filter  $(NOCACHE), 1),--no-cache)
BUILD_ARG_BASE = $(if $(filter $(NOCACHEBASE), 1),--no-cache)
NETWORK_NAME?=aboutmocking_network
DO_INSECURE?=true
GRPC_URL?=aboutmocking-grpc-server:8080
USERID?=$(shell id -u)
GROUPID?=$(shell id -g)
MIGRATION_DSN?=user=development password=development dbname=aboutmocking sslmode=disable host=db
MIGRATION_COMMAND?=status
MIGRATION_DRIVER?=postgres
MIGRATION_VOLUME?="$(shell pwd)/db/migrations"
BASE_PATH?=$(shell pwd)
DB_PORT_LOCAL?=5432
DB_TEST_PORT_LOCAL?=0.0.0.0:6432
DB_IMAGE_LOCAL?="postgres:10.10"
GRPC_LOCAL_PORT?=50051
GRPC_LOCAL_IMAGE?="aboutmocking-grpc-server:latest"
DB_LINK?="aboutmocking-db-server:db"
TEST_DIR?=$(shell pwd)
ENV_FILE?=.env
GOOSE_DRIVER?=postgres

.PHONY: all grpc_image test_image migration_image boiler_image network \
 protos_image run_database run_test_database run_tests run_generate_protos \
 shutdown_database shutdown_test_database shutdown_grpc_server

network:
	$(DOCKER) network inspect $(NETWORK_NAME) > /dev/null 2>&1 || $(DOCKER) network create $(NETWORK_NAME)
grpc_image:
	$(DOCKER) build $(BUILD_ARG) -f build/package/grpc/Dockerfile \
		--build-arg goose_driver=$(GOOSE_DRIVER) \
		--build-arg goose_dbstring="$(MIGRATION_DSN)" \
		-t aboutmocking-grpc-server .
test_image:
	$(DOCKER) build $(BUILD_ARG) \
	--build-arg goose_driver=$(GOOSE_DRIVER) \
	--build-arg goose_dbstring="$(MIGRATION_DSN)" \
	-f build/package/grpc/Dockerfile.test \
	-t aboutmocking-grpc-test-server .
migration_image:
	$(DOCKER) build $(BUILD_ARG) \
		-f build/migration/Dockerfile \
		-t aboutmocking-migration-command .
boiler_image:
	$(DOCKER) build $(BUILD_ARG) -f build/boiler/Dockerfile \
		--build-arg driver=$(MIGRATION_DRIVER) \
		-t aboutmocking-boiler-command .
protos_image:
	$(DOCKER) build $(BUILD_ARG) -f build/package/protos/Dockerfile \
	-t aboutmocking-proto-generator .
run_database:
	$(DOCKER) run --rm -d \
	--name aboutmocking-db-server -p $(DB_PORT_LOCAL):5432 \
	--env-file .env \
	-v aboutmocking_db_data:/var/lib/postgresql/data \
	--network $(NETWORK_NAME) \
	$(DB_IMAGE_LOCAL)
run_test_database:
	$(DOCKER) run --rm -d \
	--name aboutmocking-test-db-server \
	-p $(DB_TEST_PORT_LOCAL):5432 \
	--env-file .env.test \
	-v aboutmocking_test_db_data:/var/lib/postgresql/data \
	--network $(NETWORK_NAME) \
	$(DB_IMAGE_LOCAL)
run_grpc_server:
	$(DOCKER) run --rm -d \
  		--name aboutmocking-grpc-server -p $(GRPC_LOCAL_PORT):8080 \
    	--env-file .env \
    	--network $(NETWORK_NAME) \
    	--link aboutmocking-db-server:db \
    	$(GRPC_LOCAL_IMAGE)
run_migration:
	$(DOCKER) run --user $(USERID):$(GROUPID) \
		--rm -it \
		--network $(NETWORK_NAME) \
		--link $(DB_LINK) \
		--env-file $(ENV_FILE) \
		--env GOOSE_COMMAND="$(MIGRATION_COMMAND)" \
		-v $(MIGRATION_VOLUME):/go/src/aboutmocking-grpc-server/db/migrations \
		aboutmocking-migration-command
run_boiler:
	$(DOCKER) run --user $(USERID):$(GROUPID) \
		--rm -it \
		--network $(NETWORK_NAME) \
		--link aboutmocking-db-server:db \
		--env-file $(ENV_FILE) \
		-v $(BASE_PATH):/go/src/gitlab.com/jvazquez85/aboutmocking-grpc-server \
		aboutmocking-boiler-command
run_tests:
	$(DOCKER) run \
    	--rm \
    	--network $(NETWORK_NAME) \
    	--link $(DB_LINK) \
    	--env-file .env.test \
    	-v $(TEST_DIR):/go/src/gitlab.com/jvazquez85/aboutmocking-grpc-server\
    	-v $(TEST_DIR)/sqlboiler-test.toml:/go/src/gitlab.com/jvazquez85/aboutmocking-grpc-server/sqlboiler.toml \
    	aboutmocking-grpc-test-server
run_generate_protos:
	$(DOCKER) run --user $(USERID):$(GROUPID) \
		--rm -it \
		--network $(NETWORK_NAME) \
		--link $(DB_LINK) \
		-v $(BASE_PATH)/grpc:/go/src/gitlab.com/jvazquez85/aboutmocking/grpc \
	aboutmocking-proto-generator
shutdown_database:
	$(DOCKER) stop aboutmocking-db-server
	$(DOCKER) rm aboutmocking-db-server
shutdown_test_database:
	$(DOCKER) stop aboutmocking-test-db-server
	$(DOCKER) rm aboutmocking-test-db-server
shutdown_grpc_server:
	$(DOCKER) stop aboutmocking-grpc-server
	$(DOCKER) rm aboutmocking-grpc-server
volumes:
	$(DOCKER) volume create aboutmocking_db_data
	$(DOCKER) volume create aboutmocking_test_db_data